from __future__ import absolute_import, division, print_function
import numpy as np
import pandas as pd
import time
import datetime

"""
Read separate ratings and context files
"""
def data_selection(rating_filename, user_filename, sep="\t"):
    r_col_names = ["user", "item", "rate", "st"]
    df_ratings = pd.read_csv(rating_filename, sep=sep, header=None, names=r_col_names, engine='python')

    u_col_names = ['user', 'gender', 'age', 'occupation', 'zip_code']

    if (user_filename != None):
        df_users = pd.read_csv(user_filename, sep=sep, names=u_col_names, engine='python', header=None)
        df = pd.merge(df_ratings, df_users, how='left', on=['user'])
    else:
        df = df_ratings

    print(df.iloc[0]['st'])
    df["user"] -= 1
    df["item"] -= 1
    for col in ("user", "item"):
        df[col] = df[col].astype(np.int32)
        df["rate"] = df["rate"].astype(np.float32)
        df["age"] = df["age"].astype(np.int32)

    if (user_filename != None):
        print(df.info())


    #check unqiue users and items
    uniques = df.T.apply(lambda x: x.nunique(), axis=1)
    user_num = uniques["user"]
    item_num = uniques["item"]

    return df, user_num, item_num

"""
read 20 mil data file without precomputed context
"""
def data_selection_single_file(rating_filename, sep="\t"):
    r_col_names = ["user", "item", "rate", "st"]
    df = pd.read_csv(rating_filename, sep=sep, header=None, names=r_col_names, engine='python')

    df = df.drop(df.index[[0]])

    for col in ("user", "item", "age"):
        df[col] = df[col].astype(np.int32)
        df["rate"] = df["rate"].astype(np.float32)
        df["age"] = df["age"].astype(np.int32)

    print(df.info())

    uniques = df.T.apply(lambda x: x.nunique(), axis=1)
    user_num = uniques["user"]
    item_num = uniques["item"]

    return df, user_num, item_num

"""
used to read 20 mil data file context precomputed
"""
def data_selection_single_file_context(rating_filename, sep="\t"):
    r_col_names = ["user", "item", "rate", "st", "weekday", "hour", "month"]
    df = pd.read_csv(rating_filename, sep=sep, header=None, names=r_col_names, engine='python')

    df = df.drop(df.index[[0]])

    for col in ("user", "item"):
        df[col] = df[col].astype(np.int32)
        df["rate"] = df["rate"].astype(np.float32)
        df["weekday"] = df["weekday"].astype(np.int32)
        df["hour"] = df["hour"].astype(np.int32)
        df["month"] = df["month"].astype(np.int32)


    return df


"""
read 1M data fil with context precomputed
"""
def data_selection_single_file_1M_context(rating_filename, sep="\t"):
    r_col_names = ["user", "item", "rate", "st", "gender", "age", "occupation", "zip_code", "weekday", "hour", "month"]
    df = pd.read_csv(rating_filename, sep=sep, header=None, names=r_col_names, engine='python')

    df = df.drop(df.index[[0]])
    for col in ("user", "item"):
        df[col] = df[col].astype(np.int32)
        df["rate"] = df["rate"].astype(np.float32)
        df["age"] = df["age"].astype(np.int32)
        df["weekday"] = df["weekday"].astype(np.int32)
        df["hour"] = df["hour"].astype(np.int32)
        df["month"] = df["month"].astype(np.int32)

    return df

"""
Loop through file to do timestamps and precompute timestamp based context parameterse
"""
def timestampIterator(df):
    print("hi, iterating through timestamps")
    weekday = []
    hour = []
    month = []
    for i in range(0, len(df)):

        epoch = (df.iloc[i]['st'])
        d_time = datetime.datetime.fromtimestamp(int(epoch))

        weekday.append(d_time.isoweekday())
        hour.append(d_time.hour)
        month.append(d_time.month)

    df['weekday'] = weekday
    df['hour'] = hour
    df['month'] = month

    return df

"""
find out if a unix timestamp is before or after noon, DEPRECATED
"""
def timestampIsNight(epoch):
    gmtime = time.gmtime(epoch)
    return (int(time.strftime('%H', gmtime)) > 12)

"""
Remove rowss based on a criteria
"""
def removeRowsFromDF(df, criteria, column):
    df = df[column != criteria]
    return df

"""
Remove all rows that don't meet the criteria
"""
def selectRowsFromDf(df, criteria, column):
    df = df[column == criteria]
    return df

"""
Remove rowns from df based on list input.
TODO modify this method to handle the data based on input
"""
def removeCriteriaFromDF(df, criteriaList, columnList):
    if (len(criteriaList) == 0) or (len(columnList) == 0) or (len(columnList) != len(criteriaList)):
        print ("criteria or columnlist can't be empty and needs to be of the same length. Returning original dataframe")
        return df
    i = 0
    while i < len(criteriaList):
        print("removing rows of value: " + str(criteriaList[i]) + " in the " + columnList[i].name + " column")
        df = removeRowsFromDF(df, criteriaList[i], columnList[i])
        i += 1
        # print (df)
    return df

#Helper functions for selecting ranges

def occupationHelper(df,includedOccupations):
    occupations = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"]

    for includedOccupation in includedOccupations:
        occupations.remove(includedOccupation)

    tempList = ["0"]
    tempDfList = [df.occupation]
    for occupation in occupations:
        tempList[0] = occupation
        df = removeCriteriaFromDF(df, tempList, tempDfList)
    return df

def ageHelper(df, ageFloor, ageRoof):
    ages = list(range(0,100))
    del ages[ageFloor:ageRoof]
    print(ages)

    tempList = [0]
    tempDfList = [df.age]
    for age in ages:
        tempList[0] = age
        df = removeCriteriaFromDF(df, tempList, tempDfList)

    return df

def monthHelper(df, firstMonth, lastMonth):
    months = list(range(1,13))
    del months[firstMonth-1:lastMonth]
    print(months)

    tempList=[0]
    tempDfList = [df.month]
    for month in months:
        tempList[0] = month
        df = removeCriteriaFromDF(df, tempList, tempDfList)

    return df


def hourHelper(df, firstHour, lastHour):
    hours = list(range(0, 24))
    del hours[firstHour-1:lastHour]

    tempList = [0]
    tempDfList = [df.hour]
    for hour in hours:
        tempList[0] = hour
        df = removeCriteriaFromDF(df, tempList, tempDfList)

    return df

def dayHelper(df, firstWeekday, lastWeekday):
    weekdays = list(range(1, 8))
    del weekdays[firstWeekday-1:lastWeekday]
    tempList = [0]
    tempDfList = [df.weekday]
    for weekday in weekdays:
        tempList[0] = weekday
        df = removeCriteriaFromDF(df, tempList, tempDfList)

    return df

"""
Randomly generate batches
"""
class ShuffleIterator(object):
    def __init__(self, inputs, batch_size=10):
        self.inputs = inputs
        self.batch_size = batch_size
        self.num_cols = len(self.inputs)
        self.len = len(self.inputs[0])
        self.inputs = np.transpose(np.vstack([np.array(self.inputs[i]) for i in range(self.num_cols)]))

    def __len__(self):
        return self.len

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        ids = np.random.randint(0, self.len, (self.batch_size,))
        out = self.inputs[ids, :]
        return [out[:, i] for i in range(self.num_cols)]


class OneEpochIterator(ShuffleIterator):
    """
    Sequentially generate one-epoch batches, typically for test data
    """
    def __init__(self, inputs, batch_size=10):
        super(OneEpochIterator, self).__init__(inputs, batch_size=batch_size)
        if batch_size > 0:
            self.idx_group = np.array_split(np.arange(self.len), np.ceil(self.len / batch_size))
        else:
            self.idx_group = [np.arange(self.len)]
        self.group_id = 0

    def next(self):
        if self.group_id >= len(self.idx_group):
            self.group_id = 0
            raise StopIteration
        out = self.inputs[self.idx_group[self.group_id], :]
        self.group_id += 1
        return [out[:, i] for i in range(self.num_cols)]

if __name__ == '__main__':
    # df = read_process("E:/Repos/TF-recomm/tmp/movielens/ml-1m/ratings.dat", sep="::")
    # df = read_process("E:/Repos/gradu/output_timesplit.csv", sep=",")
    # df, user_num, item_num = data_selection("E:/Repos/gradu/ml-1m/ratings.dat", "E:/Repos/gradu/ml-1m/users.dat", sep="::")
    df = data_selection_single_file_1M_context("E:/Repos/gradu/1M_dataset_context.csv", sep=",")
    # df, user_num, item_num = data_selection_single_file_context("E:/Repos/gradu/ml-20m/ratings.csv", sep=",")
    print(df.info())

    # df = timestampIterator(df)
    # print (df.loc[df['user'] == 6038])

    # print (epochIterator(df))
    # print(removeRowsFromDF(df, 0, df.st))

    # df.to_csv('2076M_context.csv')

    df = dayHelper(df, 1, 5)

