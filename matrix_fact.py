try:
    import numpy
    import pandas #use pandas to handle datas
except:
    print ("This implementation requires the numpy module.")
    exit(0)

###############################################################################

"""
@INPUT:
    R     : a matrix to be factorized, dimension N x M
    P     : an initial matrix of dimension N x K
    Q     : an initial matrix of dimension M x K
    K     : the number of latent features
    steps : the maximum number of steps to perform the optimisation
    learn : the learning rate
    reg  : the regularization parameter
@OUTPUT:
    the final matrices P and Q
"""

def matrix_factorization(R, P, Q, K=3, steps=5000, learn=0.0002, reg=0.02):
    Q = Q.T
    for step in range(steps):
        for i in range(len(R)):
            for j in range(len(R[i])):
                if R[i][j] > 0:
                    eij = R[i][j] - numpy.dot(P[i,:],Q[:,j])
                    for k in range(K):
                        P[i][k] = P[i][k] + learn * (2 * eij * Q[k][j] - reg * P[i][k])
                        Q[k][j] = Q[k][j] + learn * (2 * eij * P[i][k] - reg * Q[k][j])

        error = 0
        for i in range(len(R)):
            for j in range(len(R[i])):
                if R[i][j] > 0:
                    error = error + pow(R[i][j] - numpy.dot(P[i,:],Q[:,j]), 2)
                    for k in range(K):
                        error = error + (reg / 2) * (pow(P[i][k], 2) + pow(Q[k][j], 2))
        if error < 0.001:
            break
    V = P
    W = Q
    return numpy.dot(V, W)

###############################################################################



if __name__ == "__main__":
    # R = [
    #      [5,3,0,1],
    #      [4,0,0,1],
    #      [1,1,0,5],
    #      [1,0,0,4],
    #      [0,1,5,4],
    #     ]

    R = [
            [4,0,0,5,1,0,0],
            [5,5,4,0,0,0,0],
            [0,0,0,2,4,5,0],
            [0,3,0,0,0,0,3]
        ]

    R = numpy.array(R)

    N = len(R)
    M = len(R[0])
    K = 3

    #init matrices
    P = numpy.random.rand(N,K)
    Q = numpy.random.rand(M,K)

    nR = matrix_factorization(R, P, Q, K)
    print(nR)

    # nP = numpy.matrix([
    #     [1.59419518,0.3068728],
    #     [1.4649393,1.31583012],
    #     [0.65592531,2.23078276],
    #     [0.81422458,1.1461556]])
    #
    # nQ = numpy.matrix([
    #     [1.28605964, 1.2388296],
    #     [0.58817713, 1.09751677],
    #     [0.84334312, 0.72282961],
    #     [1.20846703, 0.06060188],
    #     [0.03956435, 1.70941879],
    #     [1.37652379, 1.52482056],
    #     [1.07430918, 0.9973892]])
    #
    # nR = numpy.dot(nP, nQ.T)
    # print(nR)
    # nQ =

    # K = 2
    # P = numpy.random.rand(N, K)
    # Q = numpy.random.rand(M, K)
    #
    # nP, nQ = matrix_factorization(R, P, Q, K)
    # nR = numpy.dot(nP, nQ.T)
    # print (nR)
    #
    # K = 4
    # P = numpy.random.rand(N, K)
    # Q = numpy.random.rand(M, K)
    #
    # nP, nQ = matrix_factorization(R, P, Q, 3)
    # nR = numpy.dot(nP, nQ.T)
    # print(nR)