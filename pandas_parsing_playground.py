import pandas as pd
import numpy as np
import os
import matrix_fact as mf
# import sktensor
import time

print ("loading files")
# print (os.path.isfile("E:/repos/gradu/ml-100k/u.user"))
# print (os.path.isfile("E:/repos/gradu/ml-100k"))
# print (os.path.abspath("ml-100k/u.user"))

USERPATH100k = "ml-100k/u.item"
MOVIESPATH100k = "ml-100k/u.item"
RATINGSPATH100k = "ml-100k/u.data"

USERPATH1M = "E:/Repos/gradu/ml-1m/users.dat"
MOVIESPATH1M = "E:/Repos/gradu/ml-1m/movies.dat"
RATINGSPATH1M = "E:/Repos/gradu/ml-1m/ratings.dat"

print (os.path.isfile(USERPATH1M))

def runMatrixFactorization(numpyArray):
    N = len(numpyArray)
    M = len(numpyArray[0])
    K = 2

    P = np.random.rand(N, K)
    Q = np.random.rand(M, K)

    nP, nQ = mf.matrix_factorization(numpyArray, P, Q, K)

    nR = np.dot(nP, nQ.T)

    return nR

#matrix factorization with movielens data
def MovielensCreateNumpyArray():
    uirMatrix = ratings.pivot(index='user_id', columns='movie_id', values='rating');
    # print(uirMatrix)
    npArr = np.array(uirMatrix.values)

    return npArr

def runMockedMatrixfact():
    npArr = createMockNPArray()

    N = len(npArr)
    M = len(npArr[0])
    K = 2

    P = np.random.rand(N, K)
    Q = np.random.rand(M, K)

    nP, nQ = mf.matrix_factorization(npArr, P, Q, K)

    nR = np.dot(nP, nQ.T)

    return nR

#create a mocked array for test purposes
def createMockNPArray():
    R = [
        [5,3,0,1],
        [4,0,0,1],
        [1,1,0,5],
        [1,0,0,4],
        [0,1,5,4],
    ]

    R = np.array(R)
    return R

#create a second mocked array for test purposes
def createMockNPArrayforTensorFact():
    R = [
        [1,1,0,1],
        [1,0,0,1],
        [1,1,0,1],
        [1,0,0,1],
        [0,1,1,1],
    ]

    R = np.array(R)
    return R

#use to save output nparray as string
def saveToFile(input):
    currentTimeStamp = time.time()
    filename = "recommenderOutput-" + str(currentTimeStamp) + ".txt"

    f = open(filename, "w")
    f.write(str(input))
    f.close()

def readMovies(filepath, cols, sep):
    df = pd.read_csv(filepath, names=cols, sep=sep, engine='python', header=None, encoding='latin-1')
    return df

if __name__ == "__main__":
    print(USERPATH1M)
    # pass in column names for each CSV
    # u_cols = ['user_id', 'age', 'sex', 'occupation', 'zip_code']
    u_cols = ['user_id', 'gender', 'age', 'occupation', 'zip_code']
    users = pd.read_csv(USERPATH1M, sep='::', names=u_cols, encoding='latin-1', engine='python', header=None)

    r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
    # ratings = pd.read_csv(RATINGSPATH1M, sep='\t', names=r_cols, encoding='latin-1')
    ratings = pd.read_csv(RATINGSPATH1M, sep='::', names=r_cols, engine='python', encoding='latin-1')

    # the movies file contains columns indicating the movie's genres
    # let's only load the first five columns of the file with usecols
    m_cols = ['movie_id', 'title', 'release_date', 'video_release_date', 'imdb_url']
    # movies = pd.read_csv(MOVIESPATH1M, sep='|', names=m_cols, usecols=range(5), encoding='latin-1')
    movies = readMovies(MOVIESPATH1M, ['movie_id', 'movie name', 'genre'], '::')

    # ratings.info()
    # users.info()
    print (ratings.head())
    # # print (movies.head())
    # print (users.head())
    # print (ratings.head())
    # print (ratings.shape)
    merged = pd.merge(ratings, users, how='left', on=['user_id'])
    print (merged.head)
    print(merged[merged.user_id == 22])

    # saveToFile(runMatrixFactorization(createMockNPArray()))
