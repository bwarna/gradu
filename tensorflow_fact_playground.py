import tensorflow as tf
import numpy as np

#tensorflow playground

# hello = tf.constant('Hello, Tensorflow!')
# sess = tf.Session()
# print (sess.run(hello))
#
# # Define a 2x2 matrix in 3 different ways
# m1 = [[1.0, 2.0],
#       [3.0, 4.0]]
# m2 = np.array([[1.0, 2.0],
#                [3.0, 4.0]], dtype=np.float32)
# m3 = tf.constant([[1.0, 2.0],
#                   [3.0, 4.0]])

# # Print the type for each matrix
# print(type(m1))
# print(type(m2))
# print(type(m3))
#
# # Create tensor objects out of the different types
# t1 = tf.convert_to_tensor(m1, dtype=tf.float32)
# t2 = tf.convert_to_tensor(m2, dtype=tf.float32)
# t3 = tf.convert_to_tensor(m3, dtype=tf.float32)
#
# # Notice that the types will be the same now
# print(type(t1))
# print(type(t2))
# print(type(t3))

R = tf.Variable([
    [5.0, 3.0, 0.0, 1.0],
    [4.0, 0.0, 0.0, 1.0],
    [1.0, 1.0, 0.0, 5.0],
    [1.0, 0.0, 0.0, 4.0],
    [0.0, 1.0, 5.0, 4.0],
])
#
# print(type(R))
#
# r1 = tf.convert_to_tensor(R, dtype=tf.int32)
# print(type(r1))

sess = tf.InteractiveSession()
x_tf = tf.Variable([[0.0, 1.0, 0.0, 7.0, 0.0],
                    [2.0, 0.0, 3.0, 4.0, 5.0],
                    [4.0, 0.0, 0.0, 6.0, 7.0]
                    ])

R.initializer.run()

s, u, v = tf.svd(R, full_matrices = False)
print(u.eval())
print (u)
print(s.eval())
print(v.eval())

# testResult = tf.svd(R, compute_uv=False)

R2 = ([
    [5.0, 3.0, 0.0, 1.0],
    [4.0, 0.0, 0.0, 1.0],
    [1.0, 1.0, 0.0, 5.0],
    [1.0, 0.0, 0.0, 4.0],
    [0.0, 1.0, 5.0, 4.0],
])

R2 = np.array(R)

s1, u1, v1 = np.linalg.svd(R2, full_matrices = False)

# print(u1)
