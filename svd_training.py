import time
from collections import deque
import os

import numpy as np
import tensorflow as tf
from six import next
from tensorflow.core.framework import summary_pb2

import pandas_data_handling
import ops

np.random.seed(13575)

BATCH_SIZE = 1000 #10000
USER_NUM = 6040 #138000
ITEM_NUM = 3952 #27000
DIM = 15
EPOCH_MAX = 50
DEVICE = "/gpu:0"

DATASETFLAG20M = True

USERPATH100k = "ml-100k/u.item"
MOVIESPATH100k = "ml-100k/u.item"
RATINGSPATH100k = "ml-100k/u.data"

USERPATH1M = "E:/Repos/gradu/ml-1m/users.dat"
MOVIESPATH1M = "E:/Repos/gradu/ml-1m/movies.dat"
RATINGSPATH1M = "E:/Repos/gradu/ml-1m/ratings.dat"
RATINGSPATH1MCONTEXT = "E:/Repos/gradu/1M_dataset_context.csv"

RATINGSPATH10M = "E:/Repos/gradu/ml-10M100K/ratings.dat"
RATINGSPATH10M_4M_SPLIT = "E:/Repos/gradu/ml-10M100K/ratings_4M.dat"
RATINGSPATH10M_1M_SPLIT = "E:/Repos/gradu/ml-10M100K/ratings_1M.dat"

RATINGSPATH20M = "E:/Repos/gradu/ml-20m/ratings.csv"
RATINGSPATH20MCONTEXT =  "E:/Repos/gradu/20M_context.csv"
RATINGSPATH20MCONTEXT_SSD = "C:/gradu_dataset/20M_context.csv"



def clip(x):
    return np.clip(x, 1.0, 5.0)


def make_scalar_summary(name, val):
    return summary_pb2.Summary(value=[summary_pb2.Summary.Value(tag=name, simple_value=val)])


def get_data():

    #test runs with the 20M dataset-----------------------------------------------------
    if DATASETFLAG20M == True:

        df = pandas_data_handling.data_selection_single_file_context(RATINGSPATH20MCONTEXT_SSD, sep=",")

        # df = monthChoiceTestCase20MSummer(df)
        # df = monthChoiceTestCase20MJune(df)
        # df = monthChoiceTestCase20MWinter(df)
        # df = monthChoiceTestCase20MDecember(df)

        # df = weekdayTestCase20MWeekend(df)
        # df = weekdayTestCase20MWeekendNotFriday(df)
        # df = weekdayTestCase20MWeekday(df)
        # df = weekdayTestCase20Mfriday(df)
        df = weekdaySeason20MTestCase(df)

    # test runs with the 20M dataset-----------------------------------------------------

    # test runs with the 1M dataset-----------------------------------------------------
    else:
        df = pandas_data_handling.data_selection_single_file_1M_context(RATINGSPATH1MCONTEXT, sep=",")
        # df = ageBracketTestCase1M(df)
        # df = ageBracketTestCase1M2(df)
        # df = ageBracketTestCase1M3(df)
        # df = monthChoiceTestCase1M(df)
        # df = ageBracketMaleTestCase1M1(df)
        df = ageBracketMaleTestCase1M2(df)

    # test runs with the 1M dataset-----------------------------------------------------

    #print(df.info())

    user_num = USER_NUM
    item_num = ITEM_NUM

    rows = len(df)
    df = df.iloc[np.random.permutation(rows)].reset_index(drop=True)
    split_index = int(rows * 0.9)
    df_train = df[0:split_index]
    df_test = df[split_index:].reset_index(drop=True)
    return df_train, df_test, user_num, item_num

def svd(train, test, user_num=USER_NUM, item_num=ITEM_NUM):
    best_RMSE = 1.0
    samples_per_batch = len(train) // BATCH_SIZE

    iter_train = pandas_data_handling.ShuffleIterator([train["user"],
                                         train["item"],
                                         train["rate"]],
                                        batch_size=BATCH_SIZE)

    iter_test = pandas_data_handling.OneEpochIterator([test["user"],
                                         test["item"],
                                         test["rate"]],
                                        batch_size=-1)

    user_batch = tf.placeholder(tf.int32, shape=[None], name="id_user")
    item_batch = tf.placeholder(tf.int32, shape=[None], name="id_item")
    rate_batch = tf.placeholder(tf.float32, shape=[None])

    infer, regularizer = ops.inference_svd(user_batch, item_batch, user_num, item_num, dim=DIM,
                                           device=DEVICE)

    print (infer)

    global_step = tf.contrib.framework.get_or_create_global_step()
    _, train_op = ops.optimization(infer, regularizer, rate_batch, learning_rate=0.001, reg=0.05, device=DEVICE)

    init_op = tf.global_variables_initializer()
    with tf.Session() as session:
        session.run(init_op)
        summary_writer = tf.summary.FileWriter(logdir=os.getcwd().replace("\\", "/") + "/log", graph=session.graph)
        print("{}\t{}\t{}\t{}".format("epoch", "train_error", "val_error", "elapsed_time(s)"))
        errors = deque(maxlen=samples_per_batch)
        start = time.time()
        for i in range(EPOCH_MAX * samples_per_batch):
            users, items, rates = next(iter_train)
            _, pred_batch = session.run([train_op, infer], feed_dict={user_batch: users,
                                                                   item_batch: items,
                                                                   rate_batch: rates})
            pred_batch = clip(pred_batch)
            batch = pred_batch
            errors.append(np.power(pred_batch - rates, 2))
            if i % samples_per_batch == 0:
                train_err = np.sqrt(np.mean(errors))
                test_err2 = np.array([])
                for users, items, rates in iter_test:
                    pred_batch = session.run(infer, feed_dict={user_batch: users,
                                                            item_batch: items})
                    pred_batch = clip(pred_batch)
                    test_err2 = np.append(test_err2, np.power(pred_batch - rates, 2))
                end = time.time()
                test_err = np.sqrt(np.mean(test_err2))
                print("{:3d}\t{:f}\t{:f}\t{:f}".format(i // samples_per_batch, train_err, test_err,
                                                       end - start))
                train_err_summary = make_scalar_summary("training_error", train_err)
                test_err_summary = make_scalar_summary("test_error", test_err)
                summary_writer.add_summary(train_err_summary, i)
                summary_writer.add_summary(test_err_summary, i)
                start = end
                if best_RMSE > test_err:
                    best_RMSE = test_err


    return best_RMSE

#---testcases ---
#TODO: refactor to separate class

def onlyMalesTestCase1M(df):
    df = pandas_data_handling.removeCriteriaFromDF(df, ["F"], [df.gender])
    return df

def onlyFemalesTestCase1M(df):
    df = pandas_data_handling.removeCriteriaFromDF(df, ["M"], [df.gender])
    return df

def ageBracketTestCase1M(df):
    df = pandas_data_handling.ageHelper(df, 30, 60)
    return df

def ageBracketTestCase1M2(df):
    df = pandas_data_handling.ageHelper(df, 15, 30)
    return df

def ageBracketTestCase1M3(df):
    df = pandas_data_handling.ageHelper(df, 20, 40)
    return df

def ageBracketMaleTestCase1M1(df):
    df = pandas_data_handling.removeCriteriaFromDF(df, ["F"], [df.gender])
    df = pandas_data_handling.ageHelper(df, 20, 40)
    return df

def ageBracketMaleTestCase1M2(df):
    df = pandas_data_handling.removeCriteriaFromDF(df, ["F"], [df.gender])
    df = pandas_data_handling.ageHelper(df, 40, 60)
    return df

def monthChoiceTestCase1M(df):
    # df = pandas_data_handling.removeCriteriaFromDF(df, [1,2,3,4,5,6], [df.month, df.month,df.month,df.month,df.month,df.month,])
    df = pandas_data_handling.monthHelper(df, 4, 9)
    return df

def monthChoiceTestCase20MSummer(df):
    df = pandas_data_handling.monthHelper(df, 6, 8)
    return df

def monthChoiceTestCase20MJune(df):
    df = pandas_data_handling.monthHelper(df, 6, 6)
    return df

def weekdayTestCase20MWeekend(df):
    df = pandas_data_handling.dayHelper(df, 5,7)
    return df

def weekdayTestCase20MWeekendNotFriday(df):
    df = pandas_data_handling.dayHelper(df, 6,7)
    return df

def weekdayTestCase20MWeekday(df):
    df = pandas_data_handling.dayHelper(df, 1,5)
    return df

def weekdayTestCase20Mfriday(df):
    df = pandas_data_handling.dayHelper(df, 5,5)
    return df

def weekdaySeason20MTestCase(df):
    df = pandas_data_handling.monthHelper(df, 6, 8)
    df = pandas_data_handling.dayHelper(df, 1,5)
    return df

def monthChoiceTestCase20MDecember(df):
    df = pandas_data_handling.monthHelper(df,12,12)
    return df

def monthChoiceTestCase20MWinter(df):
    df = pandas_data_handling.removeCriteriaFromDF(df, [2,3,4,5,6,7,8,9,10], [df.month,df.month,df.month,df.month,df.month,df.month,df.month,df.month,df.month])
    return df

#--- testcases ---

if __name__ == '__main__':
    if DATASETFLAG20M == True:
        #------------------20M settings-----------------
        BATCH_SIZE = 10000
        USER_NUM = 138000
        ITEM_NUM = 27000
        #------------------20M settings-----------------

    df_train, df_test, users, items = get_data()
    rmse = svd(df_train, df_test, user_num=users, item_num=items)

    print("Finished training!, best epoch was: " + str(rmse))

    print("number of users: " + str(users))
    print("number of items:" + str(items))
    print("Batch size: " + str(BATCH_SIZE))


